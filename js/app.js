function valProm(){
    let array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    var sum = 0, prom, i;
    for (i = 0; i < array.length; i++){
        sum = sum + array[i];
    }
    prom = sum / array.length;
    console.log(prom)

    let texto = document.getElementById('prom')
    texto.innerHTML = texto.innerHTML + prom + " Esta es la cantidad promedio del arreglo: {" + array + "}";
}


function valPar(){
    let array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    var pares = 0;
    for (i = 0; i < array.length; i++){
        if((array[i] % 2) == 0){
            pares = pares + 1;
        }
    }
    console.log(pares);
    let texto = document.getElementById('par')
    texto.innerHTML = texto.innerHTML + pares + "Esta es la cantidad de numeros pares del arreglo: {" + array + "}";
}

function valM(){
    let array = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]
    var i,d,a;
    for (d = 1; d < array.length; d++){
        for (i = 0; i < (array.length - d); i++){
            if (array[i] < array[i+1]){
                a = array[i];
                array[i] = array[i+1];
                array[i+1] = a;
            }
        }
    }
    console.log(array);
    let texto = document.getElementById('orden')
    texto.innerHTML = texto.innerHTML + array + "Este es el arreglo ordenado de mayor a menor:  {" + array + "}";
}
